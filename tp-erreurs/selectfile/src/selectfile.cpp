#include "MyWindow.hpp"

#include <glog/logging.h>

int main(int argc, char ** argv) {
    // Initialize Google's logging library.
    google::InitGoogleLogging(argv[0]);

    LOG(INFO) << "On est dans main";
    
    Gtk::Main kit(argc, argv);
    MyWindow window;
    kit.run(window);
    return 0;
}

