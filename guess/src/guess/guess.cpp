#include <guess/guess.hpp>

#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

using namespace std;

guess::guess(){
    int Nombre(0);
    int Win(0);
    int Faute(0);
    int N_aleatoire(25);/*(rand() % 100 + 1);*/

    cout << "Devine un nombre entre 0 et 100 :\n" << endl;
    while(Win != 1){
        
        cout << "Entrer un nombre : ", cin >> Nombre;

        if(Nombre > 100 || Nombre < 0){
            cout << "Nombre invalide !!\n" << endl;
        }

        if (Nombre > N_aleatoire){
            cout << "Trop haut !\n" << endl;
            Faute++;
        }

        if (Nombre < N_aleatoire){
            cout << "Trop bas !\n" << endl;
            Faute++;
        }

        if (Nombre == N_aleatoire){
            cout << "Gagné !\n" << endl;
            cout << "Nombre de fautes : " << Faute << endl;
            rejouer();
        }

        if (Faute > 4){
            cout << "Perdu !\n" << endl;
            rejouer();
        }
    }
}

void guess::rejouer(){
    string Rejouer("");

    cout << "Voulez-vous rejouer ? (O/N) : \n", cin >> Rejouer ;
    if (Rejouer == "O" || Rejouer == "o")
        guess();

    if (Rejouer == "N" || Rejouer == "n"){
    }   
    else{
        cout << "Reponse invalide\n" << endl;
        rejouer();
    }
}

guess::~guess(){
}
