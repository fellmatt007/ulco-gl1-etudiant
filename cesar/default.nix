{ pkgs ? import <nixpkgs> {} }:

with pkgs; clangStdenv.mkDerivation {
    name = "cesar";
    src = ./.;

    nativeBuildInputs = [
        cmake
        doxygen
        catch2
    ];

    doCheck = true;
}


